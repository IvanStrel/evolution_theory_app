library(shiny)
library(rmarkdown)

source("functions/SimDrift.r")
source("functions/BotNeckSqr.r")
source("functions/BotNeckSim.r")
source("functions/NatSelect.r")
source("functions/HWCommonCase.r")
source("functions/HW_NatSelect.r")
source("functions/HW_inbreeding.r")
source("functions/HW_isolation.r")

Tab1 <- NULL
Tab2 <- NULL

shinyServer(function(input, output, session) {
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	#	Drift plot output
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	output$Drift_plot <- renderPlot({
		if (input$goButton == 0)
		return()
		
		isolate({
			NGen=input$NGen
			Freq=input$Freq
			PopSize=input$PopSize
			PopN=input$PopN
		})
		SimDrift(NGen=NGen, Freq=Freq, PopSize=PopSize, PopN=PopN, mode="plot")
	},
		res=68
	)
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	#	Drift hist output
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	output$Drift_hist <- renderPlot({
		if (input$goButton1 == 0)
		return()
		
		isolate({
			NGen1=input$NGen1
			Freq1=input$Freq1
			PopSize1=input$PopSize1
		})
		SimDrift(NGen=NGen1, Freq=Freq1, PopSize=PopSize1, PopN=300, mode="hist")
	},
		res=68
	)
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	#	BotNeckSqr output
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	output$BotNeckSqr <- renderPlot({
		if (input$goButton2 == 0)
		return()
		
		isolate({
			P_a=input$P_a
			P_b=input$P_b
			P_c=input$P_c
			P_d=input$P_d
			PopSize2=input$PopSize2
			BotSize=input$BotSize
		})
		BotNeckSqr(PopSize=PopSize2,BotSize=BotSize,P_a=P_a,P_b=P_b,P_c=P_c,P_d=P_d)
	},
		res=68
	)
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	#	BotNeckSim output
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	output$BotNeckSim <- renderPlot({
		if (input$goButton3 == 0)
		return()
		
		isolate({
			fA=input$Freq2
			fB=input$Freq2
			fC=input$Freq2
			fD=input$Freq2
			fE=input$Freq2
			PopSize3=input$PopSize3
			BotSize1=input$BotSize1
		})
		BotNeckSim(PopSize=PopSize3,BotSize=BotSize1,fA=fA,fB=fB,fC=fC,fD=fD,fE=fE)
	},
		res=68
	)
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	#	NatSelect output
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	output$NatSelect <- renderPlot({
		if (input$goButton4 == 0)
		return()
		
		isolate({
			PopSize4=input$PopSize4
			NGen2=input$NGen2
			PA=input$PA
			PopN1=input$PopN1
			fAA=input$fAA
			fAa=input$fAa
			faa=input$Faa
		})
		#plot(NA,xlim=c(1,1),ylim=c(1,1))
		#legend("bottom",legend=c(fAA,fAa,faa,PopSize4,NGen2,PA))
		NatSelect(PopSize=PopSize4,NGen=NGen2,PA=PA,PopN=PopN1,fAA=fAA,fAa=fAa,faa=faa)
	},
		width = 700, height = 400
	)
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	#	HWCommonCase output
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	output$HWCommonCase <- renderPlot({
		if (input$goButton5 == 0)
		return()
		
		isolate({
			PopSize5=input$PopSize5
		})
		HWCommonCase(PopSize=PopSize5)
	},
		width = 500, height = 500
	)
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	#	HW_NatSelect output
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	output$HW_NatSelect <- renderPlot({
		if (input$goButton6 == 0)
		return()
		
		isolate({
			PopSize6=input$PopSize6
			NGen3=input$NGen3
			fP=input$fP
			fAA1=input$fAA1
			fAa1=input$fAa1
			faa1=input$Faa1
		})
		HW_NatSelect(PopSize=PopSize6,NGen=NGen3,fP=fP,fAA=fAA1,fAa=fAa1,faa=faa1)
	},
		width = 700, height = 400
	)
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	#	HW_inbrieeding output
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	output$HW_inbrieeding <- renderPlot({
		if (input$goButton7 == 0)
		return()
		
		isolate({
			self_poll_ratio=input$self_poll_ratio
		})
		Tab1 <<- HW_inbrieeding(NGen = 50, fP = 0.5, PopSize = 1000, self_poll_ratio = self_poll_ratio)
	},
		width = 700, height = 400
	)
	output$table1 <- renderTable({
		if (input$goButton7 == 0)
		return()
		
		isolate({
			self_poll_ratio=input$self_poll_ratio
		})
		Tab1 <- as.data.frame(Tab1)
		colnames(Tab1) <- c("AA", "Aa", "aa")
		Tab1[length(Tab1[,1]), ]
	})
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	#	HW_isolation output
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	output$HW_isolation <- renderPlot({
		if (input$goButton8 == 0)
		return()
		
		isolate({
			fP_t6 = input$fP_t6
			Isolat = input$brink
		})
		Tab2 <<- HW_isolation(NGen=150, Isolat=Isolat, fP=fP_t6)
	},
		width = 700, height = 600
	)
	output$table2 <- renderTable({
		if (input$goButton8 == 0)
		return()
		
		isolate({
			fP_t6 = input$fP_t6
			Isolat = input$brink
		})
		Tab2 <- as.data.frame(Tab2)
		colnames(Tab2) <- c("AA", "Aa", "aa")
		rownames(Tab2) <- c("Population 1", "Population 2")
		Tab2
	})
})